# In This GitLab
**Presented in this GitLab is personal project developments, projects executed through DeVry University courses, and code snippets from programming concepts to extend programming knowledge.**

## Project Repositories
|  Project/Portfolio: 		| Description 	|  Code Base/Technologies Learned: 				|  Author or Reference: 			|
| ------------- | ------------- | :-------------: 				| ------------- 	|
| [Web Development Portfolio](https://gitlab.com/c8607/html-portfolio)	|  Projects that contain web development technologies<br>  such as HTML5, CSS, & JavaScript. 	|  ![HTML5](/images/icons8-html5-48.png) &nbsp;&nbsp; ![CSS](/images/icons8-css-48.png) &nbsp;&nbsp; ![JavaScript](./images/icons8-javascript-48.png) &nbsp;&nbsp; ![Flexbox](/images/icons8-flexbox-48.png) &nbsp;&nbsp; ![Bootstrap](/images/icons8-bootstrap-48.png) &nbsp;&nbsp; ![NodeJS](./images/icons8-node-js-48.png) &nbsp;&nbsp; ![ExpressJS](./images/icons8-express-js-48.png) &nbsp;&nbsp; ![Visual Studio Code](/images/icons8-visual-studio-code-2019-48.png) &nbsp;&nbsp; ![Wix](/images/wix-48.png) &nbsp;&nbsp; ![Pixlr](/images/pixlr.png) &nbsp;&nbsp; ![Pixlr](/images/pixlrbg.png) <br> HTML5 &nbsp;-&nbsp; CSS &nbsp;-&nbsp; JavaScript &nbsp;-&nbsp; Flexbox &nbsp;-&nbsp; Bootstrap &nbsp;-&nbsp; NodeJS &nbsp;-&nbsp; ExpressJS &nbsp;-&nbsp; Visual Studio Code &nbsp;-&nbsp; Wix  &nbsp;-&nbsp; Pixlr  &nbsp;-&nbsp; Pixlr Background Remover| Kimberly Hernandez   |
|[Collector Companion](https://gitlab.com/c8607/collector-companion)	| Application taken through SDLC to allow users<br>to keep track of a variety of in-game features<br>  that is challenges, goals, and in-game items	 |  ![Java](/images/icons8-java.gif) &nbsp;&nbsp; $\textcolor{#FF8C00}{\text{Java}}$ $\textcolor{#29465B}{\text{FX}}$  &nbsp;&nbsp;&nbsp;  ![Eclipse](/images/icons8-java-eclipse-40.png)  &nbsp;&nbsp;&nbsp;  ![SceneBuilder](/images/icons8-scene-builder-48.png) <br> Java - JavaFX - Eclipse - SceneBuilder | Kimberly Hernandez  | 
| [Landscaping Application](https://gitlab.com/c8607/landscaping-application)*	| This application allows the user to submit<br>  or retrieve landscaping orders.	|  ![Java](/images/icons8-java.gif) &nbsp;&nbsp; ![SQL](/images/icons8-sql-48.png) &nbsp;&nbsp; ![Apache NetBeans IDE 13](/images/netbeans.png) &nbsp;&nbsp; ![MySQL Workbench](/images/icons8-mysql-55.png) <br> Java &nbsp;-&nbsp; SQL - NetBeans IDE - MySQL Workbench|  Kimberly Hernandez   |
| [Friendly Advice](https://gitlab.com/c8607/friendly-advice)	| Android mobile application that generates<br>  friendly advice and has a journal feature with<br> background music. |  ![Java](/images/icons8-java.gif) &nbsp;&nbsp;&nbsp; ![Android Studio](/images/icons8-android-studio-48.png) <br> Java - Android Studio |  Kimberly Hernandez   |
| [Playlist Manager](https://gitlab.com/c8607/playlist-manager)*	| This console application allows the end-user to<br> add and view playlist information.	| ![C++](/images/icons8-c-48.png) &nbsp;&nbsp; ![Visual Studio](/images/icons8-visual-studio-48.png) &nbsp;&nbsp; ![Eclipse](/images/icons8-java-eclipse-40.png) <br> C++ &nbsp;-&nbsp; Visual Studio &nbsp;-&nbsp; Eclipse |  Kimberly Hernandez  |
| [Project Planning](https://gitlab.com/c8607/project-planning)*	|  Projects that are taken through the requirements<br>gathering and planning phases and include diagrams. 	|   ![Microsoft Word](/images/icons8-microsoft-word-2019-48.png) &nbsp;&nbsp; ![Microsoft Project](/images/icons8-microsoft-project-2019-48.png) &nbsp;&nbsp; ![Microsoft Visio](/images/icons8-microsoft-visio-2019-48.png) &nbsp;&nbsp; ![Microsoft Excel](/images/icons8-microsoft-excel-2019-48.png) &nbsp;&nbsp; ![Draw.io](/images/drawioicon.png) <br> Word &nbsp;-&nbsp; Project &nbsp;-&nbsp; Visio &nbsp;-&nbsp; Excel &nbsp;-&nbsp; Draw.IO | Kimberly Hernandez   |

*Developed as an undergrad project

## Project status
The following information is the status of the projects listed above.
- Web Development Portfolio: <i>Additional Projects coming soon.</i>
- Collector Companion: Designing Phase.
- Landscaping Application: Development Completed.
- Friendly Advice: Development Completed.
- Playlist Manager: Stopped Development.
- Project Planning: Completed. 

## General Links
[LinkedIn](https://www.linkedin.com/in/kimberly-hernandez9/) - [Wix](https://kimberlyghernandez.wixsite.com/my-site) |  [Resume](https://gitlab.com/kghernandez9/repositorypage/-/blob/main/docs/KimberlyHernandezResume.pdf) | [Contact](https://kimberlyghernandez.wixsite.com/my-site/contact) | [Presentations](https://kimberlyghernandez.wixsite.com/my-site/projects) 

<hr>

<b>Icon Links: 
- <a target="_blank" href="https://icons8.com/icon/GPfHz0SM85FX/java">Java</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/rPAHs7H1vriV/java-eclipse">Java Eclipse</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/20909/html-5">HTML5</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/21278/css3">CSS</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/9OGIyU8hrxW5/visual-studio-code-2019">Visual Studio Code 2019</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/y7WGoWNuIWac/visual-studio">Visual Studio</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/40669/c%2B%2B">C</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a> 
- <a target="_blank" href="https://icons8.com/icon/04OFrkjznvcd/android-studio">Android Studio</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/UFF3hmipmJ2V/sql">Sql</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a> 
- <a target="_blank" href="https://icons8.com/icon/9nLaR5KFGjN0/mysql-logo">mysql</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/117563/microsoft-word-2019">Microsoft Word 2019</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/117561/microsoft-excel-2019">Microsoft Excel 2019</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/7lJtplrxEIbD/microsoft-project-2019">Microsoft Project 2019</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/RFQgC8NwC8ij/microsoft-visio-2019">Microsoft Visio 2019</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/BZz399uT6eo0/scene-builder">Scene Builder</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/SosJcXuvaiLY/flexbox">Flexbox</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/ZMc42tPbG32H/bootstrap">Bootstrap</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/cQfKnWABsKk9/wix">Wix</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/hsPbhkOH4FMe/node-js">Node Js</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/2ZOaTclOqD4q/express-js">Express Js</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
- <a target="_blank" href="https://icons8.com/icon/42769/javascript-logo">Javascript</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>