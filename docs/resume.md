# **<p style="text-align: center;">Kimberly Hernandez**</p>

**<p style="text-align: center;">Seattle, WA 98105 | 206-496-5883 | Kimberly.g.hernandez9@gmail.com | https://gitlab.com/kghernandez9/repositorypage**</p>


**<p style="text-align: center;">SUMMARY**</p>

<p style="text-align: center;">Skilled Software Developer committed to ongoing growth and embracing emerging technologies. Demonstrated proficiency in adaptable collaboration and effectively juggling various responsibilities.</p>

**SKILLS**

- Programming:  OOP | Java | C++  | HTML | CSS | JavaScript | SQL |  NodeJS | ExpressJS | Bootstrap | Git CLI

- Software: MS Office | Visio | Visual Studio | Android Studio | Eclipse | NetBeans | GitLab | MySQL Workbench | Azure ML | Pixlr

- Email Marketing 
- Troubleshooting 
- Software Debugging
- Interpersonal 
- Analytical
- Organizational
- Time Management
- Problem-Solving 
- Collaborative
- Written/verbal communication

**CURRICULUM HIGHLIGHTS & PROJECTS**

<b>Introduction to Mobile Device and Programming</b>
- Applied Android software development concepts to create a mobile application, debug, implement code, and maintain the application features.

<b>Structured Analysis and Design</b>

- Applied system analysis and design to a business problem of an ineffective food ordering system, including addressing end-user requirements.

<b>Software Engineering, I & II </b>

- Collaborated with 3 classmates to apply software engineering methodologies, architectures, and debugging to a project while following the IEEE 830 Software Requirements Specification guidelines.

<b>Programming Languages and Advanced Techniques</b>

- Implemented concepts of different programming languages to solve weekly coding challenges and collaborated with classmates to address technical and software issues.

<b>Data Structures and Algorithms </b>

- Analyzed real-world speeds of several data structures and algorithms used in software and application development.

<b>Introduction to Artificial Intelligence and Machine Learning </b>

- Learned Azure Machine Learning to cover the basics of data science.

**EXPERIENCE**

<b>Freelance Wix Designer</b>

Seattle, WA (Sep 2023 –  October 2023)
-  Designed Wix website using HTML, CSS, and JavaScript knowledge, incorporating creative feedback. Developed subscriber email communications and led client meetings to discuss features, marketing, and payment systems. Implemented an attractive landing page for product/event previews, while ensuring website testing and functionality. Completed the project 2 weeks ahead of the client's deadline.

<b>Altas Palmas Animal Clinic, Front Desk Receptionist</b>

Harlingen, TX (Jan 2015 – Oct 2019)
-  Managed clinic inventory with weekly budgets ranging from 10-15 thousand, transcribed a minimum of 15 patient medical records, and handled all receptionist tasks within a 9-hour workday. 

<b>Kohl’s Department Stores, POS, Customer Service, & Jewelry Associate</b>

Harlingen, TX (Jul 2012 – Nov 2014)
- Efficiently executed responsibilities across 3 positions while exceeding customer service expectations, credit sign-up quotas, resolving cash office discrepancies under a $5 maximum, and ensuring the proper handling of defective products and reports.

**EDUCATION**

<b>DeVry University </b>

Bachelor of Science in Software Development | December 2022
- Concentration: Software Design and Programming
- Relevant coursework: Data Structures and Algorithms; Introduction to Artificial Intelligence and Machine Learning; Introduction to Mobile Device Programming; Structure Analysis and Design; Software Engineering I & II; Programming languages and Advanced Techniques
- Dean’s List; Maintained 4.0 GPA 

Associate of Applied Science in Information Technology and Networking | December 2020
- Concentration: Information Systems and Programming
- Relevant coursework: Introduction to Programming; Programming; Database Systems and Programming Fundamentals; Object-Oriented Programming
- Dean’s List; Maintained 4.0 GPA 